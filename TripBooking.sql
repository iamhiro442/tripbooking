CREATE DATABASE TripBooking;
USE TripBooking;

CREATE TABLE Carriers
(
    Id          int(96) AUTO_INCREMENT,
    Name        varchar(255) NOT NULL,
    Description varchar(255) NOT NULL,
    IsDeleted   bool,
    PRIMARY KEY (Id)
);

CREATE TABLE Points
(
    Id          int(96) AUTO_INCREMENT,
    Name        varchar(255) NOT NULL,
    Description varchar(255) NOT NULL,
    IsDeleted   bool,
    PRIMARY KEY (Id)
);

CREATE TABLE Cars
(
    Id         int(96) AUTO_INCREMENT,
    CarNumber  varchar(255) NOT NULL,
    DriverName varchar(255) NOT NULL,
    CarModel   varchar(255) NOT NULL,
    CarColor   varchar(255) NOT NULL,
    CarrierId  int(96),
    IsDeleted  bool,
    PRIMARY KEY (Id),
    FOREIGN KEY (CarrierId) REFERENCES Carriers (Id)
);

CREATE TABLE Seats
(
    Id         int(96) AUTO_INCREMENT,
    CarId      int(96),
    SeatNumber int(255) NOT NULL,
    IsDeleted  bool,
    PRIMARY KEY (Id),
    FOREIGN KEY (CarId) REFERENCES Cars (Id)
);

CREATE TABLE Trips
(
    Id               int(96) AUTO_INCREMENT,
    CarrierId        int(96),
    CarId            int(96),
    DepartureDataId  date(255) ,
    DeparturePointId int(96),
    ArrivalPointId   int(96),
    ArrivalDateTime  date(255) NOT NULL,
    Price            varchar(255) NOT NULL,
    IsActual         varchar(255) NOT NULL,
    IsDeleted        bool,
    PRIMARY KEY (Id),
    FOREIGN KEY (CarrierId) REFERENCES Carriers (Id),
    FOREIGN KEY (CarId) REFERENCES Cars (Id),
    FOREIGN KEY (DeparturePointId) REFERENCES Points (Id)
);


CREATE TABLE TripBooking
(
    Id                int(96) AUTO_INCREMENT,
    PassangerFullName varchar(255) NOT NULL,
    HasBaggage        varchar(255) NOT NULL,
    TripId            int(96),
    SeatId            int(96),
    IsDeleted         bool,
    PRIMARY KEY (Id),
    FOREIGN KEY (TripId) REFERENCES Trips (Id),
    FOREIGN KEY (SeatId) REFERENCES Seats (Id)
);
